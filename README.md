This program analyzes a large file of tweets for acrostics (words formed from each first letter of each word in a single tweet) and prints to console each acrostic found.

Please see ZIP for complete source code package, runnable JAR, resources, and design doc.