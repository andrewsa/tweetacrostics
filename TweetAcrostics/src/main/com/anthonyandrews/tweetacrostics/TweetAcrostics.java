package com.anthonyandrews.tweetacrostics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TweetAcrostics {
	static InputStream tweets;
	static InputStream words;
	static Map<String, String> acrosticTweetMap;
	static Set<String> wordSet;

	public static void main(String[] args) throws IOException {
		try {
			tweets = TweetAcrostics.class.getResourceAsStream("/tweets.txt");
			acrosticTweetMap = populateAcrosticTweetMap(tweets);
			wordSet = populateWordSet(words);
			Iterator<Map.Entry<String, String>> it = acrosticTweetMap.entrySet().iterator();
			while(it.hasNext()) {
				Map.Entry<String, String> entry = it.next();
				String acrostic = entry.getKey();
				String tweet = entry.getValue();
				if (isMatchingAcrostic(acrostic)) {
					System.out.println(acrostic + " - " + tweet);
				}
			}
		} finally {
			try {
				tweets.close();
				words.close();
			} catch (Exception ignore) {
			}
		}
	}

	// create an acrostic from each tweet to be compared to words from
	// words-lowercase.txt
	private static String createAcrostic(List<String> words) throws IOException {
		StringBuilder acrostic = new StringBuilder();
		for (int i = 0; i < words.size(); i++) {
			acrostic.append(words.get(i).charAt(0));
		}
		return acrostic.toString();
	}

	// evaluate whether a tweet contains an acrostic
	private static boolean isMatchingAcrostic(String acrosticToBeCompared) throws IOException {
		Iterator<String> wordIt = wordSet.iterator();
		while (wordIt.hasNext()) {
			if (acrosticToBeCompared.equalsIgnoreCase(wordIt.next())) {
				return true;
			}
		}
		return false;
	}

	private static Map<String, String> populateAcrosticTweetMap(InputStream tweets) throws IOException {
		Map<String, String> atMap = new HashMap<String, String>();
		BufferedReader tweetReader = null;
		try {
			tweets = TweetAcrostics.class.getResourceAsStream("/tweets.txt");
			tweetReader = new BufferedReader(new InputStreamReader(tweets));
			String line = null;
			while ((line = tweetReader.readLine()) != null) {
				line = line.trim();
				List<String> words = new ArrayList<String>();
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < line.length(); i++) {
					sb.append(line.charAt(i));
					if (line.charAt(i) == ' ' || i == line.length() - 1) {
						words.add(sb.toString());
						sb.delete(0, sb.length());
					}
				}
				String createdAcrostic = createAcrostic(words);
				if (createdAcrostic.length() >= 4) {
					atMap.put(createdAcrostic, line);
				}
			}
		} finally {
			try {
				tweets.close();
				tweetReader.close();
			} catch (Exception ignore) {
			}
		}
		return atMap;
	}

	private static Set<String> populateWordSet(InputStream words) throws IOException {
		Set<String> aSet = new HashSet<String>();
		BufferedReader wordReader = null;
		try {
			words = TweetAcrostics.class.getResourceAsStream("/words-lowercase.txt");
			wordReader = new BufferedReader(new InputStreamReader(words));
			String line = null;
			while ((line = wordReader.readLine()) != null) {
				line = line.trim();
				if (line.length() >= 4) {
					aSet.add(line);
				}
			}
		} finally {
			try {
				words.close();
				wordReader.close();
			} catch (Exception ignore) {
			}
		}
		return aSet;
	}
}